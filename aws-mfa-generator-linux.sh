#!/bin/bash

DEF_STS_PROFILE="use-profile-sts"
DEF_STS_REGION="ap-northeast-2"

set -euo pipefail

if ! command -v jq &> /dev/null
then
    JQ_PATH="${PWD}/jq"
    curl -sL -o ${JQ_PATH} "https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64"
    chmod +x ${JQ_PATH}
else
    JQ_PATH=$(which jq)
fi

while (( "$#" )); do
  case "$1" in
    --mfa-arn)
      MFA_ARN=$2
      shift 2
    ;;
    --code)
      MFA_CODE=$2
      shift 2
    ;;
    --profile)
      PROFILE=$2
      shift 2
    ;;
    -*)
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
  esac
done

if [[ -z "${MFA_ARN:-}" ]] || [[ -z "${MFA_CODE:-}" ]]; then
    echo "Must specify either --mfa-arn, --code, or --profile."
    exit 1
fi

CRED_DATA=$(aws sts get-session-token --serial-number ${MFA_ARN} --token-code ${MFA_CODE} --profile ${PROFILE:-default})

AWS_ACCESS_KEY_ID=$(${JQ_PATH} -r '.Credentials.AccessKeyId' <<< "${CRED_DATA}")
AWS_SECRET_ACCESS_KEY=$(${JQ_PATH} -r '.Credentials.SecretAccessKey' <<< "${CRED_DATA}")
AWS_SESSION_TOKEN=$(${JQ_PATH} -r '.Credentials.SessionToken' <<< "${CRED_DATA}")

aws configure set region ${DEF_STS_REGION} --profile ${DEF_STS_PROFILE}
aws configure set aws_access_key_id ${AWS_ACCESS_KEY_ID} --profile ${DEF_STS_PROFILE}
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY} --profile ${DEF_STS_PROFILE}
aws configure set aws_session_token ${AWS_SESSION_TOKEN} --profile ${DEF_STS_PROFILE}

echo ""
echo "AWS Credential Profile Generate Complete"
echo "ex) aws s3 ls --profile ${DEF_STS_PROFILE}"
echo ""

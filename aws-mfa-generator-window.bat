REM AWS Credentials with MFA

@echo off
color a
cls
echo    ___ _       _______    ______              __           __  _       __
echo   /   ^| ^|     / / ___/   / ____________  ____/ ___  ____  / /_(_____ _/ /
echo  / /^| ^| ^| // /  /  __/ /_/ /  __/ / / / /_/ / /_/ / /  ^| / /\__ \   / /   / ___/ _ \/ __  / _ \/ __ \/ __/ / __ \`/ /
                                                                                 echo / ___ ^| ^|/ ^|/ /___/ /  / /___
echo/_/  ^|_^|__/^|__//____/   \____/_/   \___/\__,_/\___/_/ /_/\__/_/\__,_/_/   
echo.                    

set DEF_STS_PROFILE="use-profile-sts"
set DEF_STS_REGION="ap-northeast-2"

WHERE jq.exe >nul 2>nul
IF %ERRORLEVEL% NEQ 0 (
	@echo jq.exe not found in path. jq downloading...

    powershell -Command "Invoke-WebRequest https://github.com/stedolan/jq/releases/download/jq-1.6/jq-win64.exe -OutFile jq.exe"
)

set /p MFA_ARN="MFA ARN: "
set /p MFA_CODE="MFA CODE: "
set /p PROFILE="PROFILE(default): "

if [%MFA_ARN%]==[] (
	echo "Must specify MFA ARN"
	exit 1
)
if [%MFA_CODE%]==[] (
	echo "Must specify MFA CODE"
	exit 1
)

if [%PROFILE%]==[] (
	set PROFILE="default"
)


>temp-sts-credential.json (
  aws sts get-session-token --serial-number %MFA_ARN% --token-code %MFA_CODE% --profile %PROFILE%
)

for /f "delims=" %%a in ('jq -r ".Credentials.AccessKeyId" temp-sts-credential.json') do set AWS_ACCESS_KEY_ID="%%a"
for /f "delims=" %%b in ('jq -r ".Credentials.SecretAccessKey" temp-sts-credential.json') do set AWS_SECRET_ACCESS_KEY="%%b"
for /f "delims=" %%c in ('jq -r ".Credentials.SessionToken" temp-sts-credential.json') do set AWS_SESSION_TOKEN="%%c"

aws configure set region %DEF_STS_REGION% --profile %DEF_STS_PROFILE%
aws configure set aws_access_key_id %AWS_ACCESS_KEY_ID% --profile %DEF_STS_PROFILE%
aws configure set aws_secret_access_key %AWS_SECRET_ACCESS_KEY% --profile %DEF_STS_PROFILE%
aws configure set aws_session_token %AWS_SESSION_TOKEN% --profile %DEF_STS_PROFILE%

del temp-sts-credential.json

@echo off
cls
echo AWS Credentials Config Successful
echo AWS Credential Profile Generate Complete
echo usage: aws s3 ls --profile %DEF_STS_PROFILE%

pause
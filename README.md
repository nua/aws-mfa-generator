# aws-mfa-generator

local 환경 aws cli 접근을 위한 mfa auto generator
매번 반복되는 aws cli 접근을 위한 mfa 발급의 시간 소요를 줄이기 위한 유틸리티
(for linux, for mac, for window)
환경별 mfa 자동 발급으로 업무 시간 대폭 단축 (aws cli 접근을 위해 보안 mfa 자동 발급)

## Installation

```
git clone https://gitlab.com/nua/aws-mfa-generator.git
```

## Getting started mac
사용 할 AWS Profile 설정 
기본 접근 Region 설정
```
DEF_STS_PROFILE="use-profile-sts"
DEF_STS_REGION="ap-northeast-2"
```
```
./aws-mfa-generator-mac.sh --mfa-arn [mfa-arn] --profile [use-profile] --code [mfa otp-code]
```

"AWS Credential Profile Generate Complete" 메세지가 나오면 mfa credential 발급 성공
aws credentials file 확인

## cli test
```
aws s3 ls --profile [use-profile]
```

## Getting started linux
사용 할 AWS Profile 설정
기본 접근 Region 설정
```
DEF_STS_PROFILE="use-profile-sts"
DEF_STS_REGION="ap-northeast-2"
```
```
./aws-mfa-generator-linux.sh --mfa-arn [mfa-arn] --profile [use-profile] --code [mfa otp-code]
```

"AWS Credential Profile Generate Complete" 메세지가 나오면 mfa credential 발급 성공
aws credentials file 확인

## cli test
```
aws s3 ls --profile [use-profile]
```

## Getting started window
사용 할 AWS Profile 설정
기본 접근 Region 설정
```
set DEF_STS_PROFILE="use-profile-sts"
set DEF_STS_REGION="ap-northeast-2"
```
```
./aws-mfa-generator-linux.sh --mfa-arn [mfa-arn] --profile [use-profile] --code [mfa otp-code]
```

"AWS Credential Profile Generate Complete" 메세지가 나오면 mfa credential 발급 성공
aws credentials file 확인

## cli test
```
aws s3 ls --profile [use-profile]
```
